﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    Vector2 velocity;
    public float smoothX = 0.5f;
    public float smoothY = 0.5f;
    public float diference = 2f;
    Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void LateUpdate()
    {
        float posX = Mathf.SmoothDamp(transform.position.x, player.position.x, ref velocity.x, smoothX);
        float posy = Mathf.SmoothDamp(transform.position.y, player.position.y + diference, ref velocity.y, smoothY);
        transform.position = new Vector3(posX, posy, transform.position.z);
    }
}
