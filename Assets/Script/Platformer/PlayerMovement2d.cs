﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2d : MonoBehaviour {

    public float playerSpeed = 5f;
    public float jumpvelocity = 10f;
    public Transform checkGround;
    public LayerMask ground;
    Rigidbody2D rb;
    CapsuleCollider2D colider;
    public bool isGrounded = false;

	void Awake () {
        rb = GetComponent<Rigidbody2D>();
        colider = GetComponent<CapsuleCollider2D>();
	}
	void Update()
    {
        if(Input.GetAxisRaw("Vertical") < 0 && Input.GetButtonDown("Jump") && isGrounded)
        {
            StartCoroutine(Down());
            return;
        }
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
    }
    void FixedUpdate() {
        isGrounded = Physics2D.Linecast(transform.position - new Vector3(0f, 0.5f, 0f), transform.position - new Vector3(0f, 0.55f, 0f), ground);
        Move(Input.GetAxisRaw("Horizontal"));
	}

    public void Move(float horizontal)
    {
        Vector2 velocity = rb.velocity;
        velocity.x = horizontal * playerSpeed;
        rb.velocity = velocity;
    }

    public void Jump()
    {
        if (isGrounded)
        {
            rb.velocity += jumpvelocity * Vector2.up;
        }
    }

    IEnumerator Down()
    {
        colider.isTrigger = true;
        rb.velocity -= (jumpvelocity / 3) * Vector2.up;
        yield return new WaitForSeconds(0.2f);
        colider.isTrigger = false;
    }
}
