﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBH : MonoBehaviour {

	public Transform door;
	public float doorSpeed = 5f;
	public float distance = 2f;
	Vector3 pos;
	Vector3 pos2;
	int openCount; 

	void Awake()
	{
		pos = door.transform.position;
		pos2 = door.transform.position + Vector3.right * distance;
	}

	void OnTriggerEnter(Collider trigger)
	{
		if (trigger.transform.tag != null) 
		{
			openCount ++;
			if (openCount == 1) 
			{
				StopAllCoroutines ();
				StartCoroutine (OpenDoor ());
			}
		}
	}

	void OnTriggerExit (Collider trigger)
	{
		if (trigger.transform.tag != null) 
		{
			openCount --;
			if (openCount == 0) 
			{
				StopAllCoroutines ();
				StartCoroutine (CloseDoor ());
			}
		}
	}

	IEnumerator OpenDoor()
	{
		while (door.transform.position.x < pos2.x) 
		{
			yield return null;
			door.position = Vector3.MoveTowards (door.position, pos2, doorSpeed * Time.deltaTime);
		}
		door.transform.position = pos2;
	}

	IEnumerator CloseDoor()
	{
		while (door.transform.position.x > pos.x) 
		{
			yield return null;
			door.position = Vector3.MoveTowards (door.position, pos, doorSpeed * Time.deltaTime);
		}
		door.transform.position = pos;
	}
}
