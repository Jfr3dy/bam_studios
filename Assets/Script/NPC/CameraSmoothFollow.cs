﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoothFollow : MonoBehaviour {
	Transform player;

	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;	
	}
	
	void LateUpdate () 
	{
		transform.position = player.transform.position - transform.forward * 20f;
	}
}
