﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Rigidbody rb;
    public float speedMovement = 5f;
	public int life;
	bool inmortal = false;
	MeshRenderer m_remder;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
		m_remder = GetComponent<MeshRenderer> ();
    }
	void Start ()
    {
		
	}

	void FixedUpdate ()
    {
        Vector3 move = (transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical"));
        Move(move);
    }

    void Move(Vector3 velocity)
    {
        rb.velocity = velocity * speedMovement;
    }

	void OnTriggerEnter(Collider trigger)
	{
		if (trigger.transform.tag == "Bullet") 
		{
			if (!inmortal) 
			{
				life -= 1;
				if (life > 0) 
				{
					StartCoroutine (Hurt ());
				} 
				else 
				{
					gameObject.SetActive (false);
					SoundManager.instance.GameOver();
				}
			}
		}
	}

	IEnumerator Hurt()
	{
		inmortal = true;
		for (int i = 0; i < 15; i++) 
		{
			if (m_remder.enabled == true) 
			{
				m_remder.enabled = false;
			} 
			else 
			{
				m_remder.enabled = true;
			}
			yield return new WaitForSeconds (0.2f); 
		}
		inmortal = false;
		m_remder.enabled = true;
	}
}
