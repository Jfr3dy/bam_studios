﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour {

	public static Path instance;
	public List<Transform> nodes = new List<Transform>();
	public Color gizmoColor = Color.black;
	Vector3 previousNode;

	void Awake()
	{
		/*
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
		*/
		for (int i = 0; i < transform.childCount; i++)
		{
			nodes.Add(transform.GetChild(i));
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = gizmoColor;
		for (int i = 0; i < transform.childCount; i++)
		{
			Vector3 currentNode = transform.GetChild(i).position;
			if(i > 0)
			{
				previousNode = transform.GetChild(i-1).position;
			}else if(i == 0)
			{
				previousNode = transform.GetChild(transform.childCount - 1).position;
			}

			Gizmos.DrawLine(currentNode, previousNode);
			Gizmos.DrawWireSphere(transform.GetChild(i).position, 0.5f);
		}
	}
}
