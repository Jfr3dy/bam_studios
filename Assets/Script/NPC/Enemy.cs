﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

	PoolBullets bullets;
	NavMeshAgent agent;
	public Transform target;
	public int index;
	public Path mPath;
	public float waitTime;
	public Transform player;
	public float distanceToChase = 10f;
	public float distanceAttack = 6f;
	Transform lastPosition;
	public float shotTime = 2;
	public float tShoot;
	float t = 0;
	public Transform m_spawnBullet;

	public GameObject found;
	public GameObject questionMark;
	AudioSource m_audio;

	public enum States
	{
		Wait,
		Move,
		Cahse,
		SearchPlayer,
		Resume
	}

	public States currentState;

	void Awake () 
	{
		m_audio = GetComponent<AudioSource> ();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		m_spawnBullet = transform.GetChild (0);
	}
	
	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		bullets = GameObject.FindObjectOfType<PoolBullets> ();
		currentState = States.Move;
		SetPathTarget ();
	}

	void Update()
	{
		bool canChase = false;
		bool canAttack = false;
		RaycastHit hit;
		Debug.DrawRay (transform.position, player.position - transform.position, Color.red);
		if (Physics.Raycast (transform.position, player.position - transform.position, out hit ,Mathf.Infinity)) 
		{
			if (hit.transform.tag == "Player") 
			{
				canChase = hit.distance < distanceToChase;
				canAttack = hit.distance < distanceAttack;
			}
		}
		float distance = Vector3.Distance (transform.position, target.position);
		if (currentState != States.Cahse && canChase) 
		{
			if (lastPosition == null)
			{
				lastPosition = transform;
			}
			currentState = States.Cahse;
			found.SetActive (true);
			questionMark.SetActive (false);
			m_audio.Stop ();
			m_audio.Play ();
            SoundManager.instance.Danger(this);
		}
		switch (currentState) 
		{
		case States.Move:
			if (distance < 0.5f) 
			{
				currentState = States.Wait;
				t = 0;
				agent.Stop ();
			}
			break;
		case States.Cahse:
			if (canChase) 
			{
				if (canAttack) 
				{
					Shoot ();
				}
				target = player.transform;
				agent.SetDestination (target.position);
				agent.Resume ();
			}
			else if(agent.remainingDistance < 0.5f)
			{
				currentState = States.SearchPlayer;
				questionMark.SetActive (true);
				found.SetActive (false);
				t = 0;
				agent.Stop ();
			}
			break;
		case States.Wait:
			t += Time.deltaTime;
			if (t >= waitTime) 
			{
				currentState = States.Move;
				SetPathTarget ();
				agent.Resume ();
			}
			break;
		case States.SearchPlayer:
			t += Time.deltaTime;
			if (t >= waitTime) 
			{
				currentState = States.Resume;
                SoundManager.instance.Normal(this);
				target = lastPosition.transform;
				agent.SetDestination (target.position);
				agent.Resume ();
			}
			break;
		case States.Resume:
			if (distance < 0.5f) 
			{
				currentState = States.Move;
				lastPosition = null;
				target = mPath.nodes [index];
				agent.SetDestination (target.position);
				agent.Resume ();
			}
			break;
		}
	}

	void SetPathTarget()
	{
		target = mPath.nodes [index];
		index++;
		if (index >= mPath.nodes.Count) 
		{
			index = 0;
		}
		agent.SetDestination (target.position);
	}

	void Shoot()
	{
		tShoot += Time.deltaTime;
		if (tShoot >= shotTime) 
		{
			tShoot = 0;
			m_spawnBullet.LookAt (player.position);
			bullets.spawnBullet = m_spawnBullet;
			bullets.Shoot ();
		}
	}
}
