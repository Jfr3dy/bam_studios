﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionBH : MonoBehaviour {

	void Awake()
	{
		gameObject.SetActive (false);
	}

	void OnEnable()
	{
		transform.localScale = Vector3.zero;
		StartCoroutine (Show ());
	}

	void OnDisable()
	{
		StopAllCoroutines ();
	}

	void LateUpdate()
	{
		transform.rotation = Camera.main.transform.rotation;
	}

	IEnumerator Show()
	{
		while (transform.localScale.x < 0.95) 
		{
			yield return null;
			transform.localScale = Vector3.MoveTowards (transform.localScale, Vector3.one, Time.deltaTime * 8);
		}
		transform.localScale = Vector3.one;
		yield return new WaitForSeconds (0.6f);

		while (transform.localScale.x > 0.05f) 
		{
			yield return null;
			transform.localScale = Vector3.MoveTowards (transform.localScale, Vector3.zero, Time.deltaTime * 8);
		}
		gameObject.SetActive (false);
	}
	
}
