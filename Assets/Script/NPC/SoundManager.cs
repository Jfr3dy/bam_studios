﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;
	public AudioClip musicBackground;
	public AudioClip musicFound;
	public AudioClip gameOver;
    AudioSource audioS;
    bool over;
    public List <Enemy> enemyList;

	void Awake()
	{
		if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        audioS = GetComponent<AudioSource>();
	}

    public  void Normal(Enemy actualEnemy)
    {
        enemyList.Remove(actualEnemy);
        if(enemyList.Count == 0)
        {
            Play(musicBackground);
        }
    }

    public void Danger(Enemy actualEnemy)
    {
        if(!enemyList.Contains(actualEnemy))
        {
            enemyList.Add(actualEnemy);
            if(enemyList.Count == 1)
            {
                Play(musicFound);
            }
        }
    }

    public void GameOver()
    {
        Play(gameOver);
        over = true;
        audioS.loop = false;
    }

    void Play(AudioClip clip)
    {
        if (!over)
        {
            audioS.clip = clip;
            audioS.Stop();
            audioS.Play();
        }
    }
}
