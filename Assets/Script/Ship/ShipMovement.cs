﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    public float movementSpeed = 5f;
    public Vector3 target;
    float limitUp;
    float limitDown;
    float limitRight;
    float limitLeft;

    void Awake()
    {
        limitUp = Camera.main.orthographicSize -0.5f;
        limitDown = (Camera.main.orthographicSize - 0.5f) * -1;
        limitRight = (Camera.main.orthographicSize -0.5f)  * Camera.main.aspect;
        limitLeft = (Camera.main.orthographicSize -0.5f) * Camera.main.aspect * -1;
    }

    void Update()
    {

        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;
        float z = 10f;
        target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, z));
        target = target - transform.position;
        float rotationAngle = Vector3.Angle(transform.up, target) * Mathf.Sign(Vector3.Cross(transform.up, target).z);
        transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + rotationAngle * Time.deltaTime * 10f);

        if (Input.GetAxis("Horizontal") != 0)
        {
            if (Input.GetAxis("Horizontal") > 0 && transform.position.x > limitRight)
            {
                return;
            }
            if (Input.GetAxis("Horizontal") < 0 && transform.position.x < limitLeft)
            {
                return;
            }
            transform.position += Vector3.right * ((movementSpeed * Input.GetAxis("Horizontal") * Time.deltaTime));
        }
        if (Input.GetAxis("Vertical") != 0)
        {
            if (Input.GetAxis("Vertical") > 0 && transform.position.y > limitUp)
            {
                return;
            }
            if (Input.GetAxis("Vertical") < 0 && transform.position.y < limitDown)
            {
                return;
            }
            transform.position += Vector3.up * ((movementSpeed * Input.GetAxis("Vertical") * Time.deltaTime));
        }
    }
}
