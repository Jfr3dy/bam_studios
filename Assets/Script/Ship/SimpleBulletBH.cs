﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBulletBH : MonoBehaviour {

    public float speed = 10f;
    public float timeLife = 5f;

    void Update()
    {
        transform.Translate(Vector3.forward * (speed * Time.deltaTime));
    }
	
    void OnEnable()
    {
        StartCoroutine(DeactiveBullet());
    }

    IEnumerator DeactiveBullet()
    {
        yield return new WaitForSeconds(timeLife);
        gameObject.SetActive(false);
    }
}
