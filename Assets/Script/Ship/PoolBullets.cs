﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolBullets : MonoBehaviour {

    public GameObject prefabBullet;
    public int bulletsAmount;
    public List<GameObject> bullets;
    public Transform spawnBullet;
	public bool isNPC;

    void Awake()
    {
        for(int i=0; i<bulletsAmount; i++)
        {
            GameObject bullet = Instantiate(prefabBullet, transform.parent = this.transform);
            bullets.Add(bullet);
            bullets[i].SetActive(false);
        }
    }

    void Update()
    {
		if (isNPC) 
		{
			return;
		}
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

     public void Shoot()
    {
        for(int i=0; i<bulletsAmount; i++)
        {
            if(bullets[i].activeSelf == false)
            {
                bullets[i].transform.position = spawnBullet.transform.position;
                bullets[i].transform.rotation = spawnBullet.transform.rotation;
                bullets[i].SetActive(true);
                break;
            }
        }
    }
}
