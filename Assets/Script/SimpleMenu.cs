﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimpleMenu : MonoBehaviour {

    public string esceneName;

	public void Ship()
    {
        SceneManager.LoadScene("Ship");
    }

    public void Platformer()
    {
        SceneManager.LoadScene("Platformer");
    }

    public void NPC()
    {
        SceneManager.LoadScene("Npc");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Menu();
        }

        if (Input.GetKey(KeyCode.R))
        {
            if (esceneName == "Ship")
            {
                Ship();
            }
            else if (esceneName == "Platformer")
            {
                Platformer();
            }
            else if (esceneName == "NPC")
            {
                NPC();
            }
        }
    }
}
